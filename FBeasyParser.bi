/'
If we don't use the initial CASTnode class and want to create another tree node struct

Function getParserJson()
(1)Get a string with JSON format
(2)Clear the CASTnode node
(3)Return the string
'/
declare function getParserJson( fullPath as string, document as string ) as string

declare function getParserNode( fullPath as string, document as string ) as any ptr

declare function getParserJsonFromExist() as string

/'
Note:
Use getParserNode() to get CASTnode ptr in our program, it must compile the same FBC compiler version between our program and DLL
V1.08 and V1.07.3 are not compatible!

V0.2:
Added this getParserNode() function

V0.3:
Modified CParser.parse() function, the function won't delete the root anymore, we need clear the node manually
It's for parsing the multiple files.


EX:
var file1RootNode = getParserNode( "1.bas", document1 )
var file2RootNode = getParserNode( "2.bas", document2 )
...
...
'*******release the memory*********
delete file1RootNode
delete file2RootNode


V0.4:
Added getParserJsonFromExist(), get JSON text from parsed tree
'/