#include once "parser.bi"
#include once "ast.bi"
#include once "tools.bi"


Constructor CParser() : end Constructor

Constructor CParser( _p as TokenUnit ptr, _tokenSize as integer )
	
	pTokens = _p
	tokenSize = _tokenSize

end Constructor

Destructor CParser() : end Destructor


sub CParser.init()

	tokenIndex = 0
	nearestTailInsertPos = 0
	jsonTXT = ""
	
end sub


sub CParser.clear()

	if( activeASTnode <> null ) then
		delete activeASTnode
		activeASTnode = null
	end if
end sub


sub CParser.setTokenData( _p as TokenUnit ptr, _tokenSize as integer )

	init()
	pTokens = _p
	tokenSize = _tokenSize
	
end sub


private function CParser.token() as TokenUnit

	if( tokenIndex < tokenSize ) then return pTokens[tokenIndex]
	return nullTokenUnit
	
end function

private function CParser.prev1() as TokenUnit

	if( tokenIndex > 0 ) then return pTokens[tokenIndex-1]
	return nullTokenUnit
	
end function

private function CParser.next1() as TokenUnit 

	if( tokenIndex < tokenSize - 1 ) then return pTokens[tokenIndex+1]
	return nullTokenUnit

end function

private function CParser.next2() as TokenUnit
	
	if( tokenIndex < tokenSize - 2 ) then return pTokens[tokenIndex+2]
	return nullTokenUnit
	
end function

private sub CParser.parseToken( t as TOK = TOK.Tidentifier )
	
	if( tokenIndex < tokenSize ) then tokenIndex += 1
	
end sub


private function CParser.parseIdentifier() as string

	dim as string ident
	
	while( token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon )
	
		if( tokenIndex >= tokenSize ) then exit while
		ident += token().identifier
		parseToken()
		
	wend
	
	return ident
end function


private function CParser.parsePreprocessor() as boolean

	static as string	_type
	
	parseToken( TOK.Tpound )

	select case( token().tok )
		
		case TOK.Tinclude
			parseToken( TOK.Tinclude )
			if( token().tok = TOK.Tonce ) then parseToken( TOK.Tonce )

			if( token().tok = TOK.Tstrings ) then 
				dim as TokenUnit t = token()
				parseToken( TOK.Tstring )
				activeASTnode->addChild( mid( str_replace( t.identifier, "\", "/" ), 2, len( trim( t.identifier ) ) - 2 ), B_INCLUDE, "", _type, "", t.lineNumber )
			end if

		case TOK.Tmacro
			parseToken( TOK.Tmacro )
			if( token().tok = TOK.Tidentifier ) then

				dim as string		_name = token().identifier
				dim as integer		lineNumber = token().lineNumber
				dim as string		paramName, paramString
				
				parseToken( TOK.Tidentifier )

				activeASTnode = activeASTnode->addChild( _name, B_MACRO, "", paramString, "", lineNumber )
				
				do while( token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon )

					paramString += token().identifier
					
					if( token().tok = TOK.Topenparen ) then
						parseToken( TOK.Topenparen )
					elseif( token().tok = TOK.Tcloseparen ) then
						parseToken( TOK.Tcloseparen )
						exit do
					elseif( token().tok = TOK.Tcomma ) then
						activeASTnode->addChild( paramName, B_PARAM, "", "", "", lineNumber )
						paramName = ""
						parseToken()
					else
						paramName += token().identifier
						parseToken()
					end if
				loop

				activeASTnode->type_ = paramString
			end if

		case TOK.Tdefine
			parseToken( TOK.Tdefine )

			if( token().tok = TOK.Tidentifier ) then
				
				dim as string	_name = token().identifier
				dim as integer	lineNumber = token().lineNumber
				parseToken( TOK.Tidentifier )

				if( token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon ) then
					if( token().tok = TOK.Tnumbers  ) then

						if( InStr( token().identifier, "." ) > 0 ) then _type = "single" else _type = "integer"
						activeASTnode->addChild( _name, B_VARIABLE OR B_DEFINE, "", _type, "", lineNumber )
						parseToken()
						
					elseif( token().tok = TOK.Tstrings  ) then
					
						activeASTnode->addChild( _name, B_VARIABLE OR B_DEFINE, "", "string", "", lineNumber )
						parseToken()
						
					elseif( token().tok = TOK.Topenparen ) then
						
						' #define GTK_VSCALE(obj) G_TYPE_CHECK_INSTANCE_CAST((obj), GTK_TYPE_VSCALE, GtkVScale)
						dim as string	param

						do while( token().tok <> TOK.Teol )
							param += token().identifier
							if(  token().tok = TOK.Tcloseparen ) then
								parseToken()
								exit do
							end if
							parseToken()
						loop
						activeASTnode->addChild( _name, B_FUNCTION OR B_DEFINE, "", param, "", lineNumber )
						parseToken()
					
					elseif( token().tok = TOK.Tidentifier ) then
						
						activeASTnode->addChild( _name, B_VARIABLE OR B_DEFINE, "", "", "", lineNumber )
						parseToken()
					end if
				end if
			end if

		case TOK.Tifdef
			parseToken( TOK.Tifdef )

			_type = ucase( token().identifier )
			parseToken()

		case TOK.Telseif, TOK.Telse
			parseToken()

			if( len( _type ) > 0 ) then
				if( chr( _type[0] ) = "!" ) then _type = ucase( right( _type, len( _type ) - 1 ) ) else	_type = ucase( "!" + _type )
			end if

		case TOK.Tendif
			parseToken( TOK.Tendif )
			_type = ""

		case TOK.Tifndef
			parseToken( TOK.Tifndef )
			_type = "!" + token().identifier
			parseToken()
			
		case else
	end select
	
	return true
	
end function

private function CParser.getVariableType() as string

	if( token().tok = TOK.Tunsigned ) then
		select case( next1().tok )
			case TOK.Tbyte:
				parseToken()
				return "ubyte"
				
			case TOK.Tshort:
				parseToken()
				return "ushort"
			
			case TOK.Tinteger
				parseToken()
				return "uinteger"
			
			case TOK.Tlongint
				parseToken()
				return "ulongint"

			case else
				parseToken()
		end select
	end if
	
	if( token().tok = TOK.Tconst ) then parseToken( TOK.Tconst )
	
	select case( token().tok )
		case TOK.Tbyte, TOK.Tubyte, TOK.Tshort,TOK.Tushort, TOK.Tinteger, TOK.Tuinteger, TOK.Tlongint, TOK.Tulongint, TOK.Tsingle, TOK.Tdouble, TOK.Tstring, TOK.Tzstring, TOK.Twstring, TOK.Tany
			return lcase( token.identifier )
		
		case TOK.Tidentifier
			dim as string _type
			do while( next1().tok = TOK.Tdot )
				_type += token.identifier
				parseToken( TOK.Tidentifier )
				_type += token.identifier
				parseToken( TOK.Tdot )
			loop

			if( len( _type ) < 1 ) then _type = token.identifier else _type += token.identifier
			return _type

		case else
	end select 
	
	return ""
	
end function

private function CParser.parseParam( bDeclare as boolean ) as string

	dim as string		_param = "("

	if( token().tok = TOK.Topenparen ) then

		parseToken( TOK.Topenparen )
		
		if( token().tok = TOK.Tcloseparen ) then
			parseToken( TOK.Tcloseparen )
			return "()"
		end if
				
		do while( token().tok <> TOK.Tcloseparen )
		
			dim as string	_name, _type
			dim as integer	_lineNum

			if( token().tok = TOK.Tbyref OR token().tok = TOK.Tbyval ) then parseToken()

			_name = token().identifier
			_lineNum = token().lineNumber

			parseToken()

			' Array
			if( token().tok = TOK.Topenparen ) then _name += parseArray()

			_param += ( _name + " " )
			
			if( token().tok = TOK.Tas ) then

				_param += ( token().identifier + " " )
				parseToken( TOK.Tas )

				' Function pointer
				if( token().tok = TOK.Tfunction OR token().tok = TOK.Tsub ) then
					dim as integer		_kind
					dim as string		__param
					
					if( token().tok = TOK.Tfunction ) then _kind = B_FUNCTION else _kind = B_SUB
					
					_param += token().identifier
					parseToken()

					if( token().tok = TOK.Tstdcall OR token().tok = TOK.Tcdecl OR token().tok = TOK.Tpascal ) then
						parseToken()
					elseif( token().tok = TOK.Tidentifier ) then ' like " Declare Function app_oninit_cb WXCALL () As wxBool "
						parseToken( TOK.Tidentifier )
					end if

					' Overload
					if( token().tok = TOK.Toverload ) then parseToken( TOK.Toverload )

					' Alias "..."
					if( token().tok = TOK.Talias ) then

						parseToken( TOK.Talias )
						if( token.tok = TOK.Tstrings ) then parseToken( TOK.Tstrings ) else return ""
						
					end if

					dim as string		_returnType
					if( token().tok = TOK.Topenparen ) then __param = parseParam( false )

					_param += __param

					if( token().tok = TOK.Tas ) then
						_param += ( " " + token().identifier )
						parseToken( TOK.Tas )

						if( token().tok = TOK.Tconst ) then
							_param += ( " " + token().identifier )
							parseToken( TOK.Tconst )
						end if

						_returnType = getVariableType()
						if( len( _returnType ) > 0 ) then 
							parseToken()
							do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
								_returnType += "*"
								parseToken()
							loop

							' Mother param strings
							_param += ( " " + _returnType )

							_type = _returnType + __param
							if( _kind = B_FUNCTION ) then activeASTnode->addChild( _name, _kind, "", _type, "", _lineNum )
						end if
					else
						if( _kind = B_SUB ) then activeASTnode->addChild( _name, _kind, "", __param, "", _lineNum ) else return ""
					end if

					if( token().tok = TOK.Tstatic OR token().tok = TOK.Texport ) then parseToken()
					
					if( token().tok = TOK.Tcomma OR token().tok = TOK.Tcloseparen ) then
						if( token().tok = TOK.Tcomma ) then 
							_param += ( token().identifier )
							parseToken( TOK.Tcomma )
						end if
					end if
					
					continue do
				end if

				if( token().tok = TOK.Tconst ) then
					_param += ( token().identifier + " " )
					parseToken( TOK.Tconst )
				end if

				_type = getVariableType()
				if( len( _type ) > 0 ) then
					
					parseToken()
					do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
						_type += "*"
						parseToken()
					loop
					_param += ( _type + " " )

					if( token().tok = TOK.Tassign ) then
						parseToken( TOK.Tassign )

						dim as integer countParen
						do
							if( token().tok = TOK.Topenparen ) then
								countParen += 1
							elseif( token().tok = TOK.Tcloseparen ) then
								countParen -= 1
								if( countParen = -1 ) then exit do 'Param Tail
							elseif( token().tok = TOK.Tcomma ) then
								if( countParen = 0 ) then exit do
							end if
							
							if( tokenIndex >= tokenSize ) then exit do
							parseToken()
							
						loop while( token().tok <> TOK.Teol )
					end if							

				
					if( token().tok = TOK.Tcomma OR token().tok = TOK.Tcloseparen ) then
						_param = trim( _param )
						
						if( bDeclare = false ) then activeASTnode->addChild( _name, B_PARAM, "", _type, "", _lineNum )
						
						if( token().tok = TOK.Tcomma ) then
							_param += ( token().identifier )
							parseToken( TOK.Tcomma )
						end if
					end if
				else
					exit do
				end if
			end if
		loop
	end if
	
	if( token().tok = TOK.Tcloseparen ) then
		parseToken( TOK.Tcloseparen )
		if( right( _param, 1 ) = " " ) then _param = left( _param, len( _param ) - 1 )

		_param = _param + ")"
	end if

	return _param

end function

private function CParser.parseArray() as string

	dim as string		result

	if( token().tok = TOK.Topenparen ) then
		
		parseToken( TOK.Topenparen )
		result = "("
		if( token().tok = TOK.Tcloseparen ) then
			parseToken( TOK.Tcloseparen )
			return "()"
		end if
		
		dim as integer	countOpenParen = 1
		do
			select case token().tok
				case TOK.Tcloseparen
					countOpenParen -= 1
					parseToken( TOK.Tcloseparen )
					result += ")"
					if( countOpenParen = 0 ) then return result

				case TOK.Topenparen
					countOpenParen += 1
					parseToken( TOK.Topenparen )
					result += "("

				case TOK.Tto:
					result += " to "
					parseToken()

				case TOK.Teol, TOK.Tcolon
					parseToken()
					return ""

				case else
					result += token.identifier
					parseToken()
			end select
		loop while( tokenIndex < tokenSize )
	end if
	
	return result
	
end function

private function CParser.parseVariable() as boolean

	dim as boolean		bConst
		
	if( token().tok = TOK.Tdim OR token().tok = TOK.Tstatic OR token().tok = TOK.Tcommon OR token().tok = TOK.Tconst OR token().tok = TOK.Tredim ) then
		
		if( token().tok = TOK.Tconst ) then bConst = true

		if( token().tok = TOK.Tredim AND next1().tok = TOK.Tpreserve ) then parseToken( TOK.Tredim )

		parseToken()

		dim as string		_type, _name, _protection
		dim as integer		_lineNum

		if( token().tok = TOK.Tshared ) then
			parseToken( TOK.Tshared )
			_protection = "shared"
		end if
		
		if( token().tok = TOK.Tas ) then

			parseToken( TOK.Tas )
			if( token().tok = TOK.Tconst ) then parseToken( TOK.Tconst )
			_type = getVariableType()
			
			if( len( _type ) > 0 ) then
				parseToken()

				if( _type = "string" OR _type = "zstring" OR _type = "wstring" ) then
					if( token().tok = TOK.Ttimes ) then
						parseToken( TOK.Ttimes )
						parseToken() ' TOK.Tnumber or TOK.Tidentifier
					end if
				end if

				do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
					_type += "*"
					parseToken()
				loop

				do while( token().tok = TOK.Tidentifier )

					_lineNum = token().lineNumber
					_name	= token().identifier
					
					parseToken( TOK.Tidentifier )

					' Array
					if( token().tok = TOK.Topenparen ) then _name += parseArray()
					
					if( token().tok = TOK.Tassign ) then

						parseToken( TOK.Tassign )

						dim as integer		countCurly, countParen
						
						do while( token.tok <> TOK.Teol AND token.tok <> TOK.Tcolon )
						
							if( token().tok = TOK.Topencurly ) then
								countCurly += 1
							elseif( token().tok = TOK.Tclosecurly ) then
								countCurly -= 1
							elseif( token().tok = TOK.Topenparen ) then
								countParen += 1
							elseif( token().tok = TOK.Tcloseparen ) then
								countParen -= 1
							end if

							if( token().tok = TOK.Tcomma ) then
								if( countParen = 0 AND countCurly = 0 ) then exit do
							end if
							
							parseToken()
							
							if( tokenIndex >= tokenSize ) then exit do
						loop					
					end if

					if( token().tok = TOK.Tcomma ) then
						activeASTnode->addChild( _name, B_VARIABLE, _protection, _type, "", _lineNum )
						parseToken( TOK.Tcomma )
						
					elseif( token().tok = TOK.Teol OR token().tok = TOK.Tcolon ) then
						activeASTnode->addChild( _name, B_VARIABLE, _protection, _type, "", _lineNum )
						parseToken()
						exit do
					else
						return false
					end if
				loop
			end if				
		
		elseif( token().tok = TOK.Tidentifier ) then
		
			do while( token().tok = TOK.Tidentifier )
				_name = token().identifier
				_lineNum = token().lineNumber
				parseToken( TOK.Tidentifier )

				if( bConst = true ) then
					if( token().tok = TOK.Tassign ) then
						parseToken( TOK.Tassign )
						if( token().tok = TOK.Tnumbers OR token().tok = TOK.Tidentifier ) then
							
							dim as boolean bSingle
							if( token().tok = TOK.Tnumbers ) then
								if( instr( token().identifier, "." ) <> 0 ) then bSingle = true
							end if
							parseToken()
							
							if( token().tok = TOK.Teol OR token().tok = TOK.Tcolon ) then
								parseToken()
								if( bSingle ) then activeASTnode->addChild( _name, B_VARIABLE, "", "single", "", _lineNum ) else activeASTnode->addChild( _name, B_VARIABLE, "", "integer", "", _lineNum )
								return true
							else
								return false
							end if
						elseif( token().tok = TOK.Tstrings ) then
							
							parseToken( TOK.Tstrings )
							if( token().tok = TOK.Teol OR token().tok = TOK.Tcolon ) then

								activeASTnode->addChild( _name, B_VARIABLE, "", "string", "", _lineNum )
								parseToken()
								return true
							else

								return false
							end if
						else

							return false
						end if
					end if
				end if
				
				' Array
				if( token().tok = TOK.Topenparen ) then _name += parseArray()
			
				if( token().tok = TOK.Tas ) then
					parseToken( TOK.Tas )

					if( token().tok = TOK.Tconst ) then parseToken( TOK.Tconst )
					
					if( parseFunctionPointer( _name, _lineNum ) ) then
						if( token.tok <> TOK.Tcomma ) then exit do else parseToken( TOK.Tcomma )
						continue do
					end if

					_type = getVariableType()
					if( len( _type ) > 0 ) then
						parseToken()

						if( _type = "string" OR _type = "zstring" OR _type = "wstring" ) then
							if( token().tok = TOK.Ttimes ) then
								parseToken( TOK.Ttimes )
								parseToken() 'TOK.Tnumber or TOK.Tidentifier
							end if
						end if

						do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
							_type += "*"
							parseToken()
						loop
						
						activeASTnode->addChild( _name, B_VARIABLE, "", _type, "", _lineNum )

						if( token.tok <> TOK.Tcomma ) then exit do else parseToken( TOK.Tcomma )
					end if
				end if
			loop
		end if
		
		return true
		
	end if

	return false

end function

private function CParser.parserVar() as boolean

	dim as string		_name, _type, _rightExpress
	dim as integer	 	_lineNum = token().lineNumber
	
	parseToken( TOK.Tvar )

	if( token().tok = TOK.Tshared ) then parseToken( TOK.Tshared )
	
	if( token().tok = TOK.Tidentifier AND next1().tok = TOK.Tassign ) then
	
		_name = token().identifier
		parseToken( TOK.Tidentifier )
		parseToken( TOK.Tassign )
		
		if( token().tok = TOK.Tnew ) then
		
			parseToken( TOK.Tnew )
			while( token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon AND token().tok <> TOK.Topenparen )
				_type += token().identifier
				parseToken()
			wend
		elseif( token().tok = TOK.Tcast ) then

			parseToken( TOK.Tcast )
			if( token().tok = TOK.Topenparen ) then
				parseToken( TOK.Topenparen )
				while( token().tok <> TOK.Tcomma AND token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon )
					_type += token().identifier							
					parseToken()
				wend
			end if
		elseif( token().tok = TOK.Tstrings ) then
			_type = "string"
			parseToken( TOK.Tstrings )
		elseif( token().tok = TOK.Tnumbers ) then
			if( instr( token().identifier, "." ) <> 0 ) then _type = "double" else _type = "integer"
			parseToken( TOK.Tnumbers )
		end if
		
		while( token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon )
			_rightExpress += token().identifier
			parseToken()
		wend
		
		dim as integer indexOpenparen = instr( _rightExpress, "(" )
		if( indexOpenparen > 0 ) then _rightExpress = left( _rightExpress, indexOpenparen - 1 )
				
		activeASTnode->addChild( _name, B_VARIABLE, "", _type, _rightExpress, _lineNum )
	else
		return false
	end if
		
	return true

end function

private function CParser.parseNamespace() as boolean

	parseToken( TOK.Tnamespace )
	if( token().tok = TOK.Tidentifier ) then
		
		dim as string splited()
		split( parseIdentifier(), splited(), "." )
		for i as integer = 0 to ubound( splited )
			activeASTnode = activeASTnode->addChild( splited(i), B_NAMESPACE, "", str(i), "", token().lineNumber )
		next

		return true
	end if

	return false
	
end function


private function CParser.parseUsing() as boolean

	parseToken( TOK.Tusing )
	
	dim as string _name
	dim as integer _lineNum
	
	while( token().tok = TOK.Tidentifier )
	
		_lineNum = token().lineNumber
		_name += token().identifier

		parseToken( TOK.Tidentifier )

		if( token().tok = TOK.Tdot ) then
		
			_name += "."
			parseToken( TOK.Tdot )
		elseif( token().tok = TOK.Tcomma ) then

			activeASTnode->addChild( _name, B_USING, "", "", "", _lineNum )
			parseToken( TOK.Tcomma )
			_name = ""
		elseif( token().tok = TOK.Teol OR token().tok = TOK.Tcolon ) then

			activeASTnode->addChild( _name, B_USING, "", "", "", _lineNum )
			parseToken()
			return true
		else
		
			return false
		end if
	wend				

	return true
	
end function


private function CParser.parseOperator( bDeclare as boolean, _protection as string ) as boolean

	dim as string		_returnType, _name, _kind, _param
	dim as integer		_lineNum, opType
	
	parseToken( TOK.Toperator )

	' Function Name
	if( bDeclare = false ) then
		if( token().tok = TOK.Tidentifier AND next1().tok = TOK.Tdot  ) then
			_name = token().identifier
			parseToken()
         
			_name += token().identifier
			parseToken( TOK.Tdot )
		end if
	end if

	select case( token().tok )

		case TOK.Tcast, TOK.Tat
		
			_name += token().identifier
			_lineNum = token().lineNumber
			parseToken()
			
			if( token().tok = TOK.Topenparen AND next1().tok = TOK.Tcloseparen ) then
				parseToken( TOK.Topenparen )
				parseToken( TOK.Tcloseparen )
			end if

			if( token().tok = TOK.Tbyref ) then
				parseToken( TOK.Tbyref )
			elseif( token().tok = TOK.Tbyval ) then
				parseToken( TOK.Tbyval )
			end if

			if( token().tok = TOK.Tas ) then
				parseToken( TOK.Tas )
				_returnType = getVariableType()
				if( len( _returnType ) > 0 ) then
					parseToken()
					do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
						_returnType += "*"
						parseToken()
					loop
				end if

				activeASTnode = activeASTnode->addChild( _name, B_OPERATOR, _protection, _returnType + "()", "", _lineNum )
				if( bDeclare = true AND activeASTnode->getFather <> 0 ) then activeASTnode = activeASTnode->getFather( token().lineNumber )
				return true
			end if

			return false

		case TOK.Topenbracket
			if( next1().tok = TOK.Tclosebracket ) then
				_lineNum = token().lineNumber
				
				parseToken( TOK.Topenbracket )
				parseToken( TOK.Tclosebracket )
				_name += "[]"
				
				if( token().tok = TOK.Topenparen ) then
					activeASTnode = activeASTnode->addChild( _name, B_OPERATOR, _protection, "", "", _lineNum )
					_param = parseParam( bDeclare )

					if( token().tok = TOK.Tbyref ) then
						parseToken( TOK.Tbyref )
					elseif( token().tok = TOK.Tbyval ) then
						parseToken( TOK.Tbyval )
					end if

					if( token().tok = TOK.Tas ) then 
						parseToken( TOK.Tas )
						_returnType = getVariableType()
						if( len( _returnType ) > 0 ) then
							parseToken()
							do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
								_returnType += "*"
								parseToken()
							loop
						end if
						
						activeASTnode->type_ = _returnType + _param
						if( bDeclare = true AND activeASTnode->getFather <> 0 ) then activeASTnode = activeASTnode->getFather( token().lineNumber )
						
						return true
					end if
				end if
			end if
			return false
			
		case TOK.Tnew, TOK.Tdelete ' "new" and "new[]" and "delete" and "delete[]"
			_lineNum = token().lineNumber
			_name += token().identifier
			parseToken()

			if( token().tok = TOK.Topenbracket AND next1().tok = TOK.Tclosebracket ) then
				parseToken( TOK.Topenparen )
				parseToken( TOK.Tcloseparen )	
				_name += "[]"
			end if

			if( token().tok = TOK.Topenparen ) then
				activeASTnode = activeASTnode->addChild( _name, B_OPERATOR, _protection, "", "", _lineNum )
				_param = parseParam( bDeclare )
				
				if( left( lcase( _name ), 3 ) = "new" ) then
					if( token().tok = TOK.Tas ) then
						parseToken( TOK.Tas )
						_returnType = getVariableType()
						if( len( _returnType ) > 0 ) then
							parseToken()
							do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
								_returnType += "*"
								parseToken()
							loop
						end if
						
						activeASTnode->type_ = _returnType + _param
						if( bDeclare = true AND activeASTnode->getFather <> 0 ) then activeASTnode = activeASTnode->getFather( token().lineNumber )
					end if
				else 'delete
					activeASTnode->type_ = _param
					if( bDeclare = true ) then activeASTnode = activeASTnode->getFather( token().lineNumber )
				end if
				
				return true
			end if
			return false
			
		case else
			if( token().tok = TOK.Tlet ) then

				if( next1().tok = TOK.Topenparen ) then opType = 1 else return false 'Check if assignment_op, p.s. opType = 1
			elseif( token().tok = TOK.Tplus OR token().tok = TOK.Tminus OR token().tok = TOK.Ttimes OR token().tok = TOK.Tandsign OR token().tok = TOK.Tdiv OR token().tok = TOK.Tintegerdiv OR token().tok = TOK.Tmod OR token().tok = TOK.Tshl _
				OR token().tok = TOK.Tshr OR token().tok = TOK.Tand OR token().tok = TOK.Tor OR token().tok = TOK.Txor OR token().tok = TOK.Timp OR token().tok = TOK.Teqv OR token().tok = TOK.Tcaret ) then

				if( next1().tok = TOK.Tassign ) then										'assignment_op		1
					if( next2().tok = TOK.Topenparen ) then opType = 1 else return false
				elseif( token().tok = TOK.Tminus AND next1().tok = TOK.Tgreater ) then		'unary_op			2
					if( next2().tok <> TOK.Topenparen ) then return false
					_name += token().identifier
					parseToken()
					opType = 2
				else																		'binary_op			3
					if( next1().tok = TOK.Topenparen ) then	opType = 3 else return false
				end if
			elseif( token().tok = TOK.Tassign OR token().tok = TOK.Tless OR token().tok = TOK.Tgreater ) then	'binary_op			3
			
				if( token().tok = TOK.Tless ) then 
					if( next1().tok = TOK.Tgreater OR next1().tok = TOK.Tassign ) then
						_name += token().identifier
						parseToken()
						opType = 3
					elseif( next1().tok = TOK.Topenparen ) then
						opType = 3
					else
						return false
					end if
				elseif( token().tok = TOK.Tgreater ) then
					if( next1().tok = TOK.Tassign ) then
						_name += token().identifier
						parseToken()
						opType = 3
					elseif( next1().tok = TOK.Topenparen ) then
						opType = 3
					else
						return false
					end if
				elseif( next1().tok = TOK.Topenparen ) then
					opType = 3
				else
					return false
				end if
		elseif( token().tok = TOK.Tnot ) then 'unary_op			2

			if( next1().tok = TOK.Topenparen ) then opType = 2 else return false
		elseif( token().tok = TOK.Tidentifier ) then 'unary_op
			
			select case( lcase( token().identifier ) )
				case "abs", "sgn", "fix", "frac", "int", "exp", "log", "sin", "asin", "cos", "acos", "tan", "atn", "len"
					if( next1().tok = TOK.Topenparen ) then opType = 2 else return false
				case else
					return false
			end select
		end if
			
		select case ( opType )
			case 1
				_name += token().identifier
				_lineNum = token().lineNumber
				parseToken()
				if( token().tok = TOK.Tassign ) then
					parseToken( TOK.Tassign )
					_name += "="
				end if

				if( token().tok = TOK.Topenparen ) then
					activeASTnode = activeASTnode->addChild( _name, B_OPERATOR, _protection, "", "", _lineNum )
					_param = parseParam( bDeclare )
					
					activeASTnode->type_ = _param
					if( bDeclare = true ) then activeASTnode = activeASTnode->getFather( token().lineNumber )
					if( token().tok = TOK.Texport ) then parseToken( TOK.Texport )
					return true
				end if
				return false

			case 2, 3
				_name += token().identifier
				_lineNum = token().lineNumber
				parseToken()

				if( token().tok = TOK.Topenparen ) then
					activeASTnode = activeASTnode->addChild( _name, B_OPERATOR, _protection, "", "", _lineNum )
					_param = parseParam( bDeclare )
					
					if( token().tok = TOK.Tbyref ) then
						parseToken( TOK.Tbyref )
					elseif( token().tok = TOK.Tbyval ) then
						parseToken( TOK.Tbyval )
					end if

					if( token().tok = TOK.Tas ) then
						parseToken( TOK.Tas )
						_returnType = getVariableType()
						if( len( _returnType ) > 0 ) then
							parseToken()
							do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
								_returnType += "*"
								parseToken()
							loop
						end if

						activeASTnode->type_ = _returnType + _param
						if( bDeclare = true ) then activeASTnode = activeASTnode->getFather( token().lineNumber )
						if( token().tok = TOK.Texport ) then parseToken( TOK.Texport )
						return true
					end if
				end if

			case else
				return false
		end select			
	end select

	return true
	
end function

private function CParser.parseProcedure( bDeclare as boolean, _protection as string ) as boolean

	dim as integer		_kind
	
	if( next1().tok = TOK.Tassign ) then
		parseToken()
		return true
	end if
	
	select case( token().tok )
		case TOK.Tfunction
			_kind = B_FUNCTION
		case TOK.Tsub
			_kind = B_SUB
		case TOK.Tproperty
			_kind = B_PROPERTY
		case TOK.Tconstructor
			_kind = B_CTOR
		case TOK.Tdestructor
			_kind = B_DTOR
	end select
	
	/'
	Think below.....
	#define RAYGUIDEF Declare
	....
	RAYGUIDEF sub GuiEnable()
	'/
	if( tokenIndex > 0 ) then
		if( prev1().tok = TOK.Tidentifier AND bDeclare = false ) then bDeclare = true
	end if
		
	if( token().tok = B_OPERATOR ) then return parseOperator( bDeclare, _protection )
	
	if( token().tok = TOK.Tfunction OR token().tok = TOK.Tsub OR token().tok = TOK.Tproperty OR token().tok = TOK.Tconstructor OR token().tok = TOK.Tdestructor ) then
		if( bDeclare = true ) then
			if( token().tok <> TOK.Tconstructor AND token().tok <> TOK.Tdestructor ) then
				parseToken()
				if( token().tok = TOK.Tstdcall OR token().tok = TOK.Tcdecl OR token().tok = TOK.Tpascal ) then parseToken()
			end if
		else
			parseToken()
		end if

		' Function Name
		if( cbool( token().tok = TOK.Tidentifier ) OR ( bDeclare = true AND cbool( token().tok = TOK.Tconstructor OR token().tok = TOK.Tdestructor ) ) ) then
			
			dim as string		_name, _param, _returnType
			dim as integer		_lineNum

			_name = token().identifier
			_lineNum = token().lineNumber
			parseToken()

			if( token().tok = TOK.Tdot AND next1().tok = TOK.Tidentifier ) then ' method

				_name += token().identifier
				parseToken( TOK.Tdot )

				_name += token().identifier
				parseToken( TOK.Tidentifier )
			end if

			if( _kind AND ( B_FUNCTION OR B_SUB ) ) then
	
				if( token().tok = TOK.Tnaked ) then parseToken( TOK.Tnaked )
				
				if( token().tok = TOK.Tstdcall OR token().tok = TOK.Tcdecl OR token().tok = TOK.Tpascal ) then
					parseToken()
				elseif( token().tok = TOK.Tidentifier ) then ' like " Declare Function app_oninit_cb WXCALL () As wxBool "
					parseToken( TOK.Tidentifier )
				end if
			end if

			' Overload
			if( token().tok = TOK.Toverload ) then parseToken( TOK.Toverload )
			
			' Lib "..."
			if( bDeclare = true ) then
				if( token().tok = TOK.Tlib ) then
					parseToken( TOK.Tlib )
					if( token.tok = TOK.Tstrings ) then parseToken( TOK.Tstrings ) else return false
				end if
			end if

			' Alias "..."
			if( token().tok = TOK.Talias ) then
				parseToken( TOK.Talias )
				if( token.tok = TOK.Tstrings ) then parseToken( TOK.Tstrings ) else return false
			end if

			activeASTnode = activeASTnode->addChild( _name, _kind, _protection, "", "", _lineNum )
			
			if( token().tok = TOK.Topenparen ) then _param = parseParam( bDeclare )
			
			' New
			if( token().tok = TOK.Tbyref ) then
				parseToken( TOK.Tbyref )
			elseif( token().tok = TOK.Tbyval ) then
				parseToken( TOK.Tbyval )
			end if

			if( token().tok = TOK.Tas ) then
				parseToken( TOK.Tas )

				if( token().tok = TOK.Tconst ) then parseToken( TOK.Tconst )

				_returnType = getVariableType()
				if( len( _returnType ) > 0 ) then
					parseToken()
					do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
						_returnType += "*"
						parseToken()
					loop
				
					activeASTnode->type_ = _returnType + _param
					if( bDeclare = true ) then activeASTnode = activeASTnode->getFather()

					if( token().tok = TOK.Tstatic ) then parseToken( TOK.Tstatic )
					
					return true
				end if
			end if

			if( token().tok = TOK.Tstatic OR token().tok = TOK.Texport OR token().tok = TOK.Toverride ) then parseToken()
			
			' SUB
			if( token().tok = TOK.Teol OR token().tok = TOK.Tcolon ) then
				activeASTnode->type_ = _param
				if( bDeclare = true AND activeASTnode->getFather <> 0 ) then activeASTnode = activeASTnode->getFather()
				return true
			end if					
		end if
	end if

	return false
	
end function


private function CParser.parseTypeBody( B_KIND as integer ) as boolean

	dim as string		_protection

	select case( B_KIND )
		case B_TYPE
			B_KIND = TOK.Ttype
		case B_UNION
			B_KIND = TOK.Tunion
		case B_ENUM
			B_KIND = TOK.Tenum
		case else
			B_KIND = TOK.Ttype
	end select
	
	do while( token().tok <> TOK.Tend AND next1().tok <> B_KIND )
	
		dim as string		_type, _name
		dim as integer		_lineNum
		
		select case( token().tok )
			case TOK.Tpublic
				parseToken( TOK.Tpublic )
				if( token().tok = TOK.Tcolon ) then
					parseToken( TOK.Tcolon )
					_protection = "public"
				end if

			case TOK.Tprivate
				parseToken( TOK.Tprivate )
				if( token().tok = TOK.Tcolon ) then
					parseToken( TOK.Tcolon )
					_protection = "private"
				end if

			case TOK.Tprotected
				parseToken( TOK.Tprivate )
				if( token().tok = TOK.Tcolon ) then
					parseToken( TOK.Tcolon )
					_protection = "protected"
				end if
				
			case TOK.Tdim
				parseVariable()
			
			case TOK.Tas
				parseToken( TOK.Tas )
				_type = getVariableType()

				if( len( _type ) > 0 ) then

					parseToken()

					if( _type = "string" OR _type = "zstring" OR _type = "wstring" ) then

						if( token().tok = TOK.Ttimes ) then
							parseToken( TOK.Ttimes )
							parseToken() ' TOK.Tnumber or TOK.Tidentifier
						end if
					end if

					do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
						_type += "*"
						parseToken()
					loop
					
					do while( token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon )
						_lineNum = token().lineNumber
						_name	= token().identifier
						
						parseToken( TOK.Tidentifier )

						' Array
						if( token().tok = TOK.Topenparen ) then _name += parseArray()
						
						' As DataType fieldname : bits [= initializer], ...
						if( token().tok = TOK.Tcolon ) then
							parseToken( TOK.Tcolon )
							if( token().tok = TOK.Tnumbers ) then parseToken( TOK.Tnumbers ) else return false
						end if							
						
						if( token().tok = TOK.Tassign ) then

							parseToken( TOK.Tassign )

							dim as integer countCurly, countParen

							do while( token.tok <> TOK.Teol AND token.tok <> TOK.Tcolon )

								if( token().tok = TOK.Topencurly ) then
									countCurly += 1
								elseif( token().tok = TOK.Tclosecurly ) then
									countCurly -= 1
								elseif( token().tok = TOK.Topenparen ) then
									countParen += 1
								elseif( token().tok = TOK.Tcloseparen ) then
									countParen -= 1
								end if

								if( token().tok = TOK.Tcomma ) then
									if( countParen = 0 AND countCurly = 0 ) then exit do
								end if
								
								parseToken()
								if( tokenIndex >= tokenSize ) then exit do
							
							loop
						end if

						if( token().tok = TOK.Tcomma ) then
							activeASTnode->addChild( _name, B_VARIABLE, _protection, _type, "", _lineNum )
							parseToken( TOK.Tcomma )
						elseif( token().tok = TOK.Teol OR token().tok = TOK.Tcolon ) then
							activeASTnode->addChild( _name, B_VARIABLE, _protection, _type, "", _lineNum )
							parseToken()
							exit do
						else
							return false
						end if
					loop					
				else
					parseToken()
				end if

			case TOK.Tstatic
				parseToken( TOK.Tstatic )

			case TOK.Tconst
				parseToken( TOK.Tconst )

			case TOK.Tdeclare:
				parseToken( TOK.Tdeclare )

				if( token().tok = TOK.Tstatic OR token().tok = TOK.Tconst OR token().tok = TOK.Tabstract OR token().tok = TOK.Tvirtual ) then parseToken()

				if( token().tok = TOK.Tfunction OR token().tok = TOK.Tsub OR token().tok = TOK.Tproperty ) then
					parseProcedure( true, _protection )
				elseif( token().tok = TOK.Tconstructor OR token().tok = TOK.Tdestructor ) then
					parseProcedure( true, _protection )
				elseif( token().tok = TOK.Toperator ) then
					parseOperator( true, _protection )
				end if

			case TOK.Teol, TOK.Tcolon
				parseToken()
				
			case TOK.Tenum
				parseEnum()
				if( token().tok = TOK.Tend OR next1().tok = TOK.Tenum ) then
					parseToken()
					parseToken()
					if( activeASTnode->getFather <> 0 ) then activeASTnode = activeASTnode->getFather( token().lineNumber )
				else
					return false
				end if

			case TOK.Tunion, TOK.Ttype:
				if( next1().tok <> TOK.Tas ) then ' If Not Variable......
				
					dim as TOK		nestUnnameTOK = token().tok
					_lineNum = token().lineNumber
					
					parseToken()
					
					if( token().tok = TOK.Tfield ) then
						if( next1().tok = TOK.Tassign ) then
							parseToken( TOK.Tfield )
							parseToken( TOK.Tassign )
							parseToken( TOK.Tnumbers )
						else
							return false
						end if
					end if			

					if( token().tok = TOK.Teol OR token().tok = TOK.Tcolon ) then

						parseToken()
						
						if( nestUnnameTOK = TOK.Tunion ) then 
							activeASTnode = activeASTnode->addChild( "", B_UNION, "", "", "", _lineNum )
						else
							activeASTnode = activeASTnode->addChild( "", B_TYPE, "", "", "", _lineNum )
						end if
						parseTypeBody( activeASTnode->kind_ )
					else
						return false
					end if
				end if
				
				_name = token().identifier
				_lineNum = token().lineNumber
				parseToken()

				' Array
				if( token().tok = TOK.Topenparen ) then _name += parseArray()

				' fieldname : bits As DataType [= initializer]
				if( token().tok = TOK.Tcolon ) then
					parseToken( TOK.Tcolon )
					if( token().tok = TOK.Tnumbers ) then parseToken( TOK.Tnumbers ) else return false
				end if
			
				if( token().tok = TOK.Tas ) then

					parseToken( TOK.Tas )
					
					if(	parseFunctionPointer( _name, _lineNum ) ) then exit select

					if( token().tok = TOK.Tconst ) then parseToken( TOK.Tconst )

					_type = getVariableType()
					if( len( _type ) > 0 ) then

						parseToken()
						
						if( _type = "string" OR _type = "zstring" OR _type = "wstring" ) then
							if( token().tok = TOK.Ttimes ) then
								parseToken( TOK.Ttimes )
								parseToken() ' TOK.Tnumber or	TOK.Tidentifier
							end if
						end if						
						
						do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
							_type += "*"
							parseToken()
						loop

						' [= initializer]
						if( token().tok = TOK.Tassign ) then
							parseToken( TOK.Tassign )
							if( token().tok = TOK.Tstring OR token().tok = TOK.Tidentifier OR token().tok = TOK.Tnumbers ) then parseToken() else return false
						end if
						activeASTnode->addChild( _name, B_VARIABLE, _protection, _type, "", _lineNum )
					end if
				else
					return false
				end if
				
			case TOK.Tpound
				parsePreprocessor()
				
			'case TOK.Tidentifier:
			case else
				_name = token().identifier
				_lineNum = token().lineNumber
				parseToken()

				' Array
				if( token().tok = TOK.Topenparen ) then _name += parseArray()

				' fieldname : bits As DataType [= initializer]
				if( token().tok = TOK.Tcolon ) then
					parseToken( TOK.Tcolon )
					if( token().tok = TOK.Tnumbers ) then parseToken( TOK.Tnumbers ) else return false
				end if
			
				if( token().tok = TOK.Tas ) then

					parseToken( TOK.Tas )
					
					if(	parseFunctionPointer( _name, _lineNum ) ) then exit select

					if( token().tok = TOK.Tconst ) then parseToken( TOK.Tconst )

					_type = getVariableType()
					if( len( _type ) > 0 ) then

						parseToken()
						
						if( _type = "string" OR _type = "zstring" OR _type = "wstring" ) then
							if( token().tok = TOK.Ttimes ) then
								parseToken( TOK.Ttimes )
								parseToken() ' TOK.Tnumber or	TOK.Tidentifier
							end if
						end if						
						
						do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
							_type += "*"
							parseToken()
						loop

						' [= initializer]
						if( token().tok = TOK.Tassign ) then
							parseToken( TOK.Tassign )
							if( token().tok = TOK.Tstring OR token().tok = TOK.Tidentifier OR token().tok = TOK.Tnumbers ) then parseToken() else return false
						end if
						activeASTnode->addChild( _name, B_VARIABLE, _protection, _type, "", _lineNum )
					end if
				else
					return false
				end if

		end select
	loop

	' After exit loop......
	if( token().tok = TOK.Tend AND next1().tok = B_KIND ) then
		parseToken()
		parseToken()
		activeASTnode = activeASTnode->getFather( token().lineNumber )
		return true
	end if

	return false

end function

private function CParser.parseFunctionPointer( _name as string, _lineNumber  as integer ) as boolean

	dim as string	_param, _type
	dim as integer	_kind
	
	' Function pointer
	if( token().tok = TOK.Tfunction OR token().tok = TOK.Tsub ) then

		if( token().tok = TOK.Tfunction ) then _kind = B_FUNCTION else _kind = B_SUB 
		parseToken()

		if( token().tok = TOK.Tstdcall OR token().tok = TOK.Tcdecl OR token().tok = TOK.Tpascal ) then
			parseToken()
		elseif( token().tok = TOK.Tidentifier ) then ' like " Declare Function app_oninit_cb WXCALL () As wxBool "
			parseToken( TOK.Tidentifier )
		end if

		' Overload
		if( token().tok = TOK.Toverload ) then parseToken( TOK.Toverload )

		' Alias "..."
		if( token().tok = TOK.Talias ) then
			parseToken( TOK.Talias )
			if( token.tok = TOK.Tstrings ) then parseToken( TOK.Tstrings ) else return false
		end if

		dim as string  _returnType

		if( token().tok = TOK.Topenparen ) then _param = parseParam( true )

		if( token().tok = TOK.Tas ) then
		
			parseToken( TOK.Tas )

			if( token().tok = TOK.Tconst ) then parseToken( TOK.Tconst )

			_returnType = getVariableType()
			if( len( _returnType ) > 0 ) then
				parseToken()
				do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
					
					_returnType += "*"
					parseToken()
				loop

				_type = _returnType + _param
				activeASTnode->addChild( _name, _kind, "", _type, "", _lineNumber )
				
				return true
			end if
		end if

		if( token().tok = TOK.Tstatic OR token().tok = TOK.Texport OR token().tok = TOK.Toverride ) then parseToken()
		
		if( token().tok = TOK.Teol OR token().tok = TOK.Tcolon ) then ' SUB
			activeASTnode->addChild( _name, _kind, "", _param, "", _lineNumber )
			return true
		end if
	end if
	
	return false

end function
	
private function CParser.parseType( bClass as boolean = false ) as boolean

	if( tokenIndex > 0 ) then
		if( prev1().tok = TOK.Tdot OR prev1().tok = TOK.Tptraccess ) then
			parseToken()
			return false
		end if
	end if

	dim as string		_name, _param, _type, _base
	dim as integer		_lineNum, _kind

	
	if( cbool( token().tok = TOK.Ttype ) OR cbool( token().tok = TOK.Tunion ) OR ( bClass = true AND cbool( token().tok = TOK.Tclass ) ) ) then
		select case( token().tok )
			case TOK.Tunion
				_kind = B_UNION
			case TOK.Ttype
				_kind = B_TYPE
			case TOK.Tclass
				_kind = B_CLASS
			case else
		end select
		
		parseToken()

		_name = token().identifier
		_lineNum = token().lineNumber

		parseToken( TOK.Tidentifier )

		if( token().tok = TOK.Tas ) then 
			parseToken( TOK.Tas )
			
			if( parseFunctionPointer( _name, _lineNum ) ) then return true

			if( token().tok = TOK.Tconst ) then parseToken( TOK.Tconst )

			_type = getVariableType()
			if( len( _type ) > 0 ) then 
				parseToken()

				do while( token().tok = TOK.Tptr OR token().tok = TOK.Tpointer )
					_type += "*"
					parseToken()
				loop
			end if

			if( token().tok = TOK.Tcolon OR token().tok = TOK.Teol ) then
				activeASTnode->addChild( _name, B_ALIAS, "", _type, "", _lineNum )
				return true
			end if
		else
			if( token().tok = TOK.Textends ) then
				parseToken( TOK.Textends )
				if( token().tok = TOK.Tidentifier OR token().tok = TOK.Tobject ) then _base = parseIdentifier()
				/'
				_base = token().identifier
				parseToken( TOK.Tidentifier )
				'/
			end if

			if( token().tok = TOK.Tfield ) then
				if( next1().tok = TOK.Tassign ) then 
					parseToken( TOK.Tfield )
					parseToken( TOK.Tassign )
					parseToken( TOK.Tnumbers )
				else
					return false
				end if
			end if

			if( token().tok = TOK.Teol ) then
				if( bClass = true ) then
					activeASTnode = activeASTnode->addChild( _name, B_CLASS, "", "", _base, _lineNum )
				else
					activeASTnode = activeASTnode->addChild( _name, _kind, "", "", _base, _lineNum )
				end if
				parseToken( TOK.Teol )
				parseTypeBody( _kind )
			end if
		end if
		
		return true
	end if

	return false

end function 

private function CParser.parseEnumBody() as boolean

	dim as string 	_name, _param, _type, _base
	dim as integer	_lineNum
	
	do while( token().tok <> TOK.Tend AND next1().tok <> TOK.Tenum )
		
		if( token().tok = TOK.Tidentifier ) then

			_name = token().identifier
			_lineNum = token().lineNumber
			parseToken( TOK.Tidentifier )

			if( token().tok = TOK.Tassign ) then 
				parseToken( TOK.Tassign )
				' if( token().tok == TOK.Tidentifier || token().tok == TOK.Tnumbers ) parseToken(); else break;
			end if

			' Pass the maybe complicated express
			'while( token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon )
			while( token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon AND token().tok <> TOK.Tcomma )
				if( tokenIndex >= tokenSize ) then exit do
				parseToken()
			wend

			if( token().tok = TOK.Teol OR token().tok = TOK.Tcolon OR token().tok = TOK.Tcomma ) then
				activeASTnode->addChild( _name, B_ENUMMEMBER, "", "", "", _lineNum )
				parseToken()
			end if
		else
			exit do
		end if
		
	loop

	return false
end function

private function CParser.parseEnum() as boolean

	dim as string	_name, _param, _type, _base
	dim as integer	_lineNum

	if( token().tok = TOK.Tenum ) then

		parseToken( TOK.Tenum )

		if( token().tok = TOK.Tidentifier ) then
			_name = token().identifier
			parseToken( TOK.Tidentifier )
		end if

		_lineNum = token().lineNumber

		if( token().tok = TOK.Teol ) then
			activeASTnode = activeASTnode->addChild( _name, B_ENUM, "", "", _base, _lineNum )
			parseToken( TOK.Teol )
			parseEnumBody()
		end if
		
		return true
		
	end if

	return false

end function

private function CParser.parseScope() as boolean

	parseToken( TOK.Tscope )
	
	if( token().tok = TOK.Teol OR token().tok = TOK.Tcolon ) then
		activeASTnode = activeASTnode->addChild( "", B_SCOPE, "", "", "", token().lineNumber )
	else
		return false
	end if

	return true
	
end function	

private function CParser.parseWith() as boolean

	parseToken( TOK.Twith )

	dim as string user_defined_var
		
	do

		user_defined_var += token().identifier
		parseToken()
	loop while( token().tok <> TOK.Teol AND token().tok <> TOK.Tcolon )
	activeASTnode = activeASTnode->addChild( user_defined_var, B_WITH, "", "", "", token().lineNumber, -1 )
	
	return false

end function

private function CParser.parseEnd() as boolean

	parseToken( TOK.Tend )
	
	select case( token().tok )
		case TOK.Tsub, TOK.Tfunction, TOK.Tproperty, TOK.Toperator, TOK.Tconstructor, TOK.Tdestructor, TOK.Ttype, TOK.Tenum, TOK.Tunion, TOK.Tscope, TOK.Twith, TOK.Tclass
			if( activeASTnode->getFather() <> 0 ) then activeASTnode = activeASTnode->getFather( token().lineNumber )
			parseToken()
			return true
		
		case TOK.Tnamespace
			if( activeASTnode->kind_ AND B_NAMESPACE ) then
			
				dim as integer loopUpperLimit = valint( activeASTnode->type_ )
				for i as integer = 0 to loopUpperLimit
					
					if( activeASTnode->getFather() <> null ) then activeASTnode = activeASTnode->getFather( token().lineNumber )
				next
			end if
			parseToken()

		case else
			'parseToken()
			
	end select

	return false

end function

function CParser.parse( fullPath as string, B_KIND as integer = 0 ) as CASTnode ptr

	dim as integer	dotPos = inStrRev( fullPath, "." ) 
	
	/' Comment since V0.3
	if( activeASTnode <> null ) then
		delete activeASTnode
		activeASTnode = null
	end if
	'/
	
	'if( dotPos = 0 ) then return null
	
	dim as CASTnode ptr		head
	dim as string			_ext = lcase( right( fullPath, dotPos - 1 ) )
	
	fullPath = str_replace( fullPath, "\", "/" )

	if( _ext = "bas" ) then
		head = new CASTnode( fullPath, B_BAS, "", "", "", 0, 2147483647 )
	else
		head = new CASTnode( fullPath, B_BI, "", "", "", 0, 2147483647 )
	end if

	activeASTnode = head

	do while( tokenIndex < tokenSize )
		
		select case ( pTokens[tokenIndex].tok  )
			case TOK.Tprivate
				parseToken( TOK.Tprivate )
				if( token().tok = TOK.Tsub OR token().tok = TOK.Tfunction ) then parseProcedure( false, "private" )

			case TOK.Tprotected
				parseToken( TOK.Tprotected )
				if( token().tok = TOK.Tsub OR token().tok = TOK.Tfunction ) then parseProcedure( false, "protected" )
				
			case TOK.Tpublic
				parseToken( TOK.Tpublic )
				if( token().tok = TOK.Tsub OR token().tok = TOK.Tfunction ) then parseProcedure( false, "public" )
			
			case TOK.Tpound
				parsePreprocessor()

			case TOK.Tconst, TOK.Tcommon, TOK.Tstatic, TOK.Tdim, TOK.Tredim
				parseVariable()

			case TOK.Tvar
				parserVar()
				
			case TOK.Tsub
				if( next1().tok = TOK.Tidentifier AND next2().tok = TOK.Tcomma ) then
					' ASM Command
					parseToken( TOK.Tsub )
				else
					parseProcedure( false, "" )
				end if

			case TOK.Tfunction, TOK.Tproperty, TOK.Tconstructor, TOK.Tdestructor
				parseProcedure( false, "" )
				
			case TOK.Toperator
				parseOperator( false, "" )

			case TOK.Tend
				parseEnd()

			case TOK.Ttype
				parseType()

			case TOK.Tclass
				parseType( true )

			case TOK.Tunion
				parseType()
				
			case TOK.Tenum
				parseEnum()

			case TOK.Tscope
				parseScope()
				
			case TOK.Twith
				parseWith()

			case TOK.Tnamespace
				parseNamespace()
				
			case TOK.Tusing
				if( next1().tok <> TOK.Tstrings AND next1().tok = TOK.Tidentifier ) then parseUsing()
				
			case TOK.Tdeclare
				parseToken( TOK.Tdeclare )
				
				if( activeASTnode->kind_ AND ( B_TYPE OR B_CLASS ) ) then
				
					if( token().tok = TOK.Tstatic ) then
					
						parseToken( TOK.Tstatic )
						if( token().tok = TOK.Tvirtual ) then parseToken( TOK.Tvirtual )
					elseif( token().tok = TOK.Tvirtual ) then

						parseToken( TOK.Tvirtual )
						if( token().tok = TOK.Tstatic ) then parseToken( TOK.Tstatic )
					end if
				end if				
				
				if( token().tok = TOK.Tfunction OR token().tok = TOK.Tsub OR token().tok = TOK.Tconstructor OR token().tok = TOK.Tdestructor OR token().tok = TOK.Tproperty ) then 
					parseProcedure( true, "" )
				elseif( token().tok = TOK.Toperator ) then
					parseOperator( true, "" )
				end if
				
			case TOK.Tassign
				parseToken( TOK.Tassign )

			case TOK.Tendmacro
				activeASTnode = activeASTnode->getFather( token().lineNumber )
				tokenIndex += 1

			case else
				tokenIndex += 1
				'Stdout( tokenIndex );
				'Stdout( "   " ~ token().identifier ).newline;
		end select
		
	loop
	
	if( activeASTnode <> head ) then head->endLineNum_ = 2147483646 else head->endLineNum_ = 2147483647
	
	activeASTnode = head
	
	return head
end function


private sub CParser.ASTtoJSON( _node as CASTnode ptr )

	jsonTXT += "{" + chr(10)
	jsonTXT +=	"""line"": " + """" + str( _node->lineNumber_ )	+ """," + chr(10)
	jsonTXT +=	"""tail"": " + """" + str( _node->endLineNum_ )	+ """," + chr(10)	
	jsonTXT +=	"""name"": " + """" + _node->name_				+ """," + chr(10)
	jsonTXT +=	"""kind"": " + """" + str( _node->kind_ )		+ """," + chr(10)
	jsonTXT +=	"""prot"": " + """" + _node->protection_	+ """," + chr(10)
	jsonTXT +=	"""type"": " + """" + _node->type_			+ """," + chr(10)
	jsonTXT +=	"""base"": " + """" + _node->base_			+ """," + chr(10)
	
	if( _node->getChildrenCount = 0 ) then 
		jsonTXT +=	"""sons"": " + "[]" + chr(10)
	else
		jsonTXT +=	"""sons"": " + "[" + chr(10)
		
		for i as integer = 0 to _node->getChildrenCount - 1
			
			ASTtoJSON( _node->getChild( i ) )
			if( i < _node->getChildrenCount - 1 ) then jsonTXT += "," + chr(10)
		
		next
		jsonTXT +=	"]" + chr(10)
	end if
	
	jsonTXT += "}"
end sub


function CParser.toJSON() as string

	jsonTXT = ""

	if( activeASTnode <> null ) then ASTtoJSON( activeASTnode )
	
	return jsonTXT
end function