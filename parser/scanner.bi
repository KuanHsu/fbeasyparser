#include once "token.bi"
#include once "tools.bi"


type Scanner
	private :
		as TokenUnit		_tokens(any)
	
		declare sub scan2Token( fileData as string )
	
	public :
		declare Constructor()
		declare Constructor( document as string )
		declare Destructor()
		
		declare function scan( document as string ) as TokenUnit ptr
		declare function getTokenCount() as integer

end type