#include once "fbc-int/array.bi"

#define null 0

' This function by counting_pine
' https://www.freebasic.net/forum/viewtopic.php?t=8730
declare function str_replace( byref haystack as string, byref needle1 as string, byref needle2 as string ) as string

' This function by fxm
' https://www.freebasic.net/forum/viewtopic.php?p=284817#p284817
declare Function split(Byref s As Const String, result() As String, Byref delimiter As Const String = ",") As Integer
declare Function join(s() As Const String, Byref delimiter As Const String = ",") As String