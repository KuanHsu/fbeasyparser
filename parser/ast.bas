#include once "ast.bi"

constructor CASTnode( __name as string, __kind as integer, __protection as string, __type as string, __base as string, __lineNumber as integer, __endLineNum as integer = -1 ) export

	name_ = __name
	kind_ = __kind
	protection_ = __protection
	type_ = __type
	base_ = __base
	lineNumber_ = __lineNumber
	if( __endLineNum = -1 ) then endLineNum_ = __lineNumber else endLineNum_ = __endLineNum

	'print "Create: "; __name

end constructor

destructor CASTnode() export

	for i as integer = 0 to childrenSize - 1
		if( children(i) <> null ) then delete children(i)
	next
	
end destructor


operator CASTnode.[] ( index as integer ) as CASTnode ptr export

	if( index < childrenSize ) then return children(index) else return null
	
end operator


function CASTnode.addChild( _child as CASTnode ptr ) as integer export

	_child->father = @this
	redim preserve children(childrenSize)
	children(childrenSize) = _child
	childrenSize += 1
	
	return childrenSize - 1

end function

function CASTnode.addChild( __name as string, __kind as uinteger, __protection as string, __type as string, __base as string, __lineNumber as integer, __endLineNum as integer = -1 ) as CASTnode ptr export

	dim as CASTnode ptr		_child = new CASTnode( __name, __kind, __protection, __type, __base, __lineNumber, __endLineNum )

	' Set the father node
	_child->father = @this

	' Add new child node to tail
	redim preserve children( childrenSize )
	children(childrenSize) = _child
	childrenSize += 1

	return _child
	
end function
	
function CASTnode.getChild( index as integer ) as CASTnode ptr export

	if( index < childrenSize ) then return children(index)
	
	return null

end function

function CASTnode.child( index as integer ) as CASTnode ptr export

	return getChild(index)

end function

function CASTnode.getFather( __endLineNum as integer = -1 ) as CASTnode ptr export

	if( __endLineNum > 0 ) then endLineNum_ = __endLineNum
	
	return father
	
end function

function CASTnode.getChildren() as CASTnode ptr export

	return children(0)

end function

function CASTnode.getChildrenCount() as integer export
	
	return childrenSize
	
end function