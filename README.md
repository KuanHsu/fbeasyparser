# FBeasyParser

This little DLL tool can generate a json format document for freeBASIC syntax parsed tree, it could be used in codecompletion

## How to use:

(1) Include the FBeasyParser.bi, link with the libFBeasyParser.dll.a

(2) Example code:

```bas
#include once "FBeasyParser.bi"
#inclib "FBeasyParser"


'************* Start *************
dim as string document

document = "dim as integer i, j, k" + chr(10)
document += "dim as string word = Kuan Hsu"

dim as string json = getParserJson( "test.bas", document )
print json
sleep
```

The output is:

```json
{
"line": "0",
"tail": "2147483647",
"name": "test.bas",
"kind": "32768",
"prot": "",
"type": "",
"base": "",
"sons": [
{
"line": "1",
"tail": "1",
"name": "i",
"kind": "1",
"prot": "",
"type": "integer",
"base": "",
"sons": []
},
{
"line": "1",
"tail": "1",
"name": "j",
"kind": "1",
"prot": "",
"type": "integer",
"base": "",
"sons": []
},
{
"line": "1",
"tail": "1",
"name": "k",
"kind": "1",
"prot": "",
"type": "integer",
"base": "",
"sons": []
},
{
"line": "2",
"tail": "2",
"name": "word",
"kind": "1",
"prot": "",
"type": "string",
"base": "",
"sons": []
}]
}
```

* line= Start line number
* tail= End line number; in root node(kind= B_BI/B_BAS), tail= 2147483647 is mean parsing completely, tail= 2147483646 is mean not complete
* name= Parsed node name
* kind= Parsed node kind ( check "ast.bi" )
* prot= Public/ Protected / Private
* type= The type of parsed node, if kind is B_SUB/B_FUNCTION/B_CTOR, it also with the parameters, like **type(xx as integer,xxx as string)**
* base= Mother class name
* sons= Children parserd node