﻿#include once "parser/scanner.bi"
#include once "parser/parser.bi"
#include once "FBeasyParser.bi"

dim shared as Scanner 						tokenScanner
dim shared as CParser 	 					parser


'=============================
'=============================
' DLL functions
function getParserJson( fullPath as string, document as string ) as string export
	
	dim as TokenUnit ptr pTokens = tokenScanner.scan( document )
	if( pTokens <> null ) then

		parser.setTokenData( pTokens, tokenScanner.getTokenCount )
		dim as CASTnode ptr root = parser.parse( fullPath )
		dim as string result = parser.toJSON()
		
		parser.clear()
		
		return result
	end if

	return ""
end function


function getParserNode( fullPath as string, document as string ) as any ptr export
	
	dim as TokenUnit ptr pTokens = tokenScanner.scan( document )
	if( pTokens <> null ) then

		parser.setTokenData( pTokens, tokenScanner.getTokenCount )
		return parser.parse( fullPath )
	end if

	return null
end function


function getParserJsonFromExist() as string export
	
	return parser.toJSON()
end function